C:
:: working directory
@echo off
set adir=C:\Users\%username%\Desktop\test
cd %adir%
cls
curl https://gitlab.com/nstoc6696/rhost/-/raw/main/ctools/cputester/corecode.bat > ecode.txt
curl -O https://gitlab.com/nstoc6696/rhost/-/raw/main/ctools/ncmd/invis.vbs

:: how many running engines
set /p tenum="How many cores?"
set /a tenum=%tenum%+1
set /a dnum=1

:start
if %dnum% EQU %tenum% goto esc
if not exist e%dnum%.bat echo. > e%dnum%.bat
set /a dnum=%dnum%+1
goto start

:esc
set /a dnum=1
:start1
if %dnum% EQU %tenum% goto check
type ecode.txt > e%dnum%.bat
set /a dnum=%dnum%+1
goto start1

:check
set /p start="Start the benchmark? :"
if %start% EQU Y goto launch

:launch
del ecode.txt
set /a dnum=1
:start2
if %dnum% EQU %tenum% goto complete
wscript.exe "invis.vbs" e%dnum%.bat
set /a dnum=%dnum%+1
goto start2

:complete
echo DONE!

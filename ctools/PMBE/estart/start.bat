C:
:: working directory
set adir=""
cd %adir%
:: how many running engines
set /a tenum=5
set dnum=1
:emake
if not exist (e%dnum%.bat echo %dnum% > e%dnum%.bat && goto check)
:check
if exist e%dnum%.bat echo e%dnum% > tmpfile && set /p echeck= < tmpfile && del tmpfile
if %echeck% EQU %tenum% (echo. > e%dnum%.bat && goto rewrite) else (echo. > e%dnum%.bat set /a dnum=%dnum%+1 && goto emake)

:: code to be written to engines
echo code.txt > tmpfile && set code= < tmpfile && del tmpfile

:: write code to engines
:rewrite
if exist e%tenum%.bat (goto loop1) else (goto emake)
set dnum=1
:loop1
del e%dnum%.bat
echo %code% > e%dnum%.bat
set set /a dnum=%dnum%+1
if %dnum% EQU %tenum% goto escape1
goto loop1

:escape1

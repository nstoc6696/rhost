:: curl sucks lol
@echo off
cls
echo this is n installer
set /p dir="please set your save directory: "
cd %dir%
echo "your save directory was set to: %dir%" 
cls

:browsers
echo "please enter your answer for yes as y"
set /p browser="Would you like to install any browsers?"
if %browser% EQU y (
    set /p chrome="Download Chome installer? :"
    if %chrome% EQU y *curl chromeinstaller*
    set /p firefox="Download Firefox installer? :"
    if %firefox% EQU y *curl firefoxinstaller*
    set /p opera="Download Opera installer? :"
    if %opera% EQU y *curl operainstaller*
    set /p operagx="Download Opera GX installer? :"
    if %operagx% EQU y *curl operagxinstaller*
    set /p brave="Download Brave installer? :"
    if %brave% EQU y *curl braveinstaller*
    set /p duckduck="Download DuckDuckGo installer? :"
    if %duckduck% EQU y *curl duckduckgoinstaller*
    goto gaming
) else (
    goto gaming
)

:gaming
cls
echo "please enter your answer for yes as y"
set /p gplat="Would you like to install any Game platforms?"
if %gplat% EQU y (
    set /p steam="Download Steam installer? :"
    if %steam% EQU y *curl steaminstaller*
    set /p epic="Download Epic Games installer? :"
    if %epic% EQU y *curl epicinstaller*
    set /p uplay="Download Uplay installer? :"
    if %uplay% EQU y *curl uplayinstaller*
    goto productivity
) else (
    goto productivity
)

:productivity
cls
echo "please enter your answer for yes as y"
set /p pdt="Would you like to install any productivity programs?"
if %pdt% EQU y (
    set /p cc="Download Creative Cloud installer? :"
    if %cc% EQU y *curl ccinstaller*
    set /p gdrive="Download Google Drive installer? :"
    if %gdrive% EQU y *curl gdriveinstaller*
    set /p db="Download Dropbox installer? :"
    if %db% EQU y *curl dbinstaller*
    goto games
) else (
    goto games
)

:games
cls
echo "please enter your answer for yes as y"
set /p instgames="Would you like to install any Games?"
if %instgames% EQU y (
    set /p mc="Download Minecraft installer? :"
    if %mc% EQU y *curl mcinstaller*
    goto comms
) else (
    goto comms
)

:comms
cls
echo "please enter your answer for yes as y"
set /p mg="Would you like to install any messaging programs?"
if %mg% EQU y (
    set /p discord="Download Discord installer? :"
    if %discord% EQU y *curl discordinstaller*
    set /p skype="Download Skype installer? :"
    if %skype% EQU y *curl installer*
    goto remote
) else (
    goto remote
)

:remote
cls
echo "please enter your answer for yes as y"
set /p rc="Would you like to install any Remote Control Software?"
if %rc% EQU y (
    set /p rpc="Download RemotePC installer? :"
    if %rpc% EQU y *curl rpcinstaller*
    set /p crd="Download Chrome Remote Desktop installer? :"
    if %crd% EQU y *curl crdinstaller*
    goto mda
) else (
    goto mda
)

:mda
cls
echo "please enter your answer for yes as y"
set /p media="Would you like to install any Media Software?"
if %media% EQU y (
    set /p nppp="Download Notepad++ installer? :"
    if %nppp% EQU y *curl np++installer*
    set /p vlc="Download VLC installer? :"
    if %vlc% EQU y *curl vlcinstaller*
    set /p ad="Download Audacity installer? :"
    if %ad% EQU y *curl adinstaller*
    set /p spot="Download Spotify installer? :"
    if %spot% EQU y *curl spotifyinstaller*
    set /p hb="Download Handbrake installer? :"
    if %hb% EQU y *curl handbrakeinstaller*
    set /p obs="Download OBS installer? :"
    if %obs% EQU y *curl obsinstaller*
    goto rgb
) else (
    goto rgb
)

:rgb
cls
echo "please enter your answer for yes as y"
set /p hc="Would you like to install any RGB / Home Contol sofware?"
if %hc% EQU y (
    set /p ic="Download ICUE installer? :"
    if %ic% EQU y *curl icinstaller*
    set /p rs="Download Razer Synapse installer? :"
    if %rs% EQU y *curl rsinstaller*
    set /p sd="Download Steam Deck installer? :"
    if %sd% EQU y *curl sdinstaller*
    goto runt
) else (
    goto runt
)

:runt
cls
echo "please enter your answer for yes as y"
set /p rt="Would you like to install any Runtimes/Envioments?"
if %rt% EQU y (
    set /p vsc="Download Visual Studio Code installer? :"
    if %vsc% EQU y *curl vscinstaller*
    set /p eclipse="Download Eclipse installer? :"
    if %eclipse% EQU y *curl eclipseinstaller*
    set /p intelij="Download Intellij installer? :"
    if %intelij% EQU y *curl intelijinstaller*
    set /p jdk="Download JDK installer? :"
    if %jdk% EQU y *curl jdkinstaller*
    set /p net="Download .net installer? :"
    if %net% EQU y *curl netinstaller*
    set /p ="Download installer? :"
    if %% EQU y *curl installer*
    goto compr
) else (
    goto compr
)

:compr
cls
echo "please enter your answer for yes as y"
set /p cmpr="Would you like to install any Compression Software?"
if %cmpr% EQU y (
    set /p sevenzip="Download 7Zip installer? :"
    if %sevenzip% EQU y *curl sevenzipinstaller*
    set /p wr="Download WinRar installer? :"
    if %wr% EQU y *curl WRinstaller*
    goto *next*
) else (
    goto *next*
)
